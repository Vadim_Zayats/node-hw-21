import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import { Observable } from 'rxjs';
import * as dotenv from 'dotenv';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor() {
    dotenv.config();
  }

  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const requestApiKey = context.switchToHttp().getRequest().headers.api_key;
    const apiKey = process.env.API_KEY;

    if (!apiKey) {
      console.log('GUARD - NO API KEY');
      return false;
    }
    if (requestApiKey !== apiKey) {
      console.log('GUARD - NOT RIGHT KEY');
      return false;
    }
    console.log('GUARD - OK');
    return true;
  }
}
