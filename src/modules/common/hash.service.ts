import { Injectable } from '@nestjs/common';

@Injectable()
export class CommonService {
  EncodeString(str: string): string {
    return Buffer.from(str).toString('base64');
  }

  DecodeString(str: string): string {
    return Buffer.from(str, 'base64').toString('utf-8');
  }
}
