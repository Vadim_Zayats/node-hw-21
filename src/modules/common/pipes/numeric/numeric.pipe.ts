import {
  ArgumentMetadata,
  Injectable,
  PipeTransform,
  BadRequestException,
} from '@nestjs/common';

@Injectable()
export class NumericPipe implements PipeTransform {
  transform(value: any, metadata: ArgumentMetadata) {
    const isNumeric = /^\d+$/.test(value);

    if (!isNumeric) {
      throw new BadRequestException(`Invalid number value '${value}'`);
    }
    return value;
  }
}
