import { Module } from '@nestjs/common';
import { CommonService } from './hash.service';

@Module({
  controllers: [],
  providers: [CommonService],
})
export class CommonModule {}
