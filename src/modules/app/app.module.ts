import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { DbModule } from 'src/modules/db/db.module';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { RecordsModule } from '../records/records.module';
import { CommonModule } from 'src/modules/common/common.module';

@Module({
  imports: [
    RecordsModule,
    CommonModule,
    ConfigModule.forRoot({ isGlobal: true }),
    DbModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
