import { Module } from '@nestjs/common';
import { RecordService } from './records.service';
import { RecordController } from './records.controller';
import { CommonService } from 'src/modules/common/hash.service';

@Module({
  controllers: [RecordController],
  providers: [RecordService, CommonService],
})
export class RecordsModule {}
