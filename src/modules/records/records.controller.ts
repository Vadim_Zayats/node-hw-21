import {
  Controller,
  Get,
  Post,
  Put,
  Delete,
  Body,
  Param,
  UseGuards,
  UseFilters,
  UsePipes,
  UseInterceptors,
} from '@nestjs/common';
import { RecordService } from './records.service';
import { CreateRecordDto } from './dto/create-record.dto';
import { AuthGuard } from 'src/auth/auth.guard';
import { GlobalExceptionFilter } from 'src/errors/global.filter';
import { ForbiddenExceptionFilter } from 'src/errors/forbidden.filter';
import { LoggingInterceptor } from './logging.interceptor';
import { ValidationPipe } from 'src/modules/common/pipes/validation/validation.pipe';
import { NumericPipe } from 'src/modules/common/pipes/numeric/numeric.pipe';

@UseGuards(AuthGuard)
@UseFilters(ForbiddenExceptionFilter)
@UseFilters(GlobalExceptionFilter)
@UseInterceptors(LoggingInterceptor)
@Controller('records')
export class RecordController {
  constructor(private readonly recordService: RecordService) {}

  @Post()
  @UsePipes(new ValidationPipe())
  createRecord(@Body() record: CreateRecordDto): CreateRecordDto {
    return this.recordService.createRecord(record);
  }

  @Get()
  getAllRecords(): CreateRecordDto[] {
    return this.recordService.getAllRecords();
  }

  @Get(':id')
  getRecordById(@Param('id', NumericPipe) id: number): CreateRecordDto {
    return this.recordService.getRecordById(id);
  }

  @Get(':id/decoded')
  getDecodedRecordContent(@Param('id', NumericPipe) id: number) {
    return this.recordService.decodedRecordContent(id);
  }

  @Put(':id')
  updateRecord(
    @Param('id', NumericPipe) id: number,
    @Body() record: CreateRecordDto,
  ): CreateRecordDto {
    return this.recordService.updateRecord(id, record);
  }

  @Delete(':id')
  deleteRecord(@Param('id', NumericPipe) id: number): CreateRecordDto {
    return this.recordService.deleteRecord(id);
  }
}
