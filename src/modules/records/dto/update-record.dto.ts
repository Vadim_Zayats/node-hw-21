import { PartialType } from '@nestjs/mapped-types';
import { CreateRecordDto } from './create-record.dto';
import { IsNumber } from 'class-validator';
export class UpdateRecordDto extends PartialType(CreateRecordDto) {
  @IsNumber()
  id: number;
}
