import { IsString } from 'class-validator';

export class CreateRecordDto {
  id: number;
  @IsString()
  name: string;
  @IsString()
  content: string;
}
