import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateRecordDto } from './dto/create-record.dto';
import { CommonService } from 'src/modules/common/hash.service';
@Injectable()
export class RecordService {
  private records: CreateRecordDto[] = [];

  constructor(private readonly commonService: CommonService) {}

  createRecord(record: CreateRecordDto): CreateRecordDto {
    const encodedContent = this.commonService.EncodeString(record.content);
    const newRecord = { ...record, content: encodedContent };
    this.records.push(newRecord);
    return newRecord;
  }

  getAllRecords(): CreateRecordDto[] {
    return this.records;
  }

  getRecordById(id: number): CreateRecordDto {
    const record = this.records.find((record) => record.id === Number(id));
    if (!record) {
      throw new NotFoundException(`Record with ID ${id} not found`);
    }
    return record;
  }

  decodedRecordContent(id: number) {
    const record = this.getRecordById(id);
    const decodedContent = this.commonService.DecodeString(record.content);
    const decodedRecord = {
      ...record,
      content: decodedContent,
    };
    return decodedRecord;
  }

  updateRecord(id: number, record: CreateRecordDto): CreateRecordDto {
    const index = this.records.findIndex((r) => r.id === id);
    if (index !== -1) {
      // Encode content to base64 before saving
      const encodedContent = Buffer.from(record.content).toString('base64');
      const updatedRecord = { ...record, content: encodedContent };
      this.records[index] = updatedRecord;
      return updatedRecord;
    }
    return null;
  }

  deleteRecord(id: number): CreateRecordDto {
    const index = this.records.findIndex((record) => record.id === id);
    if (index !== -1) {
      const deletedRecord = this.records.splice(index, 1)[0];
      return deletedRecord;
    }
    return null;
  }
}
