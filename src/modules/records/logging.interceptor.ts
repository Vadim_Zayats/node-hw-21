import {
  Injectable,
  NestInterceptor,
  ExecutionContext,
  CallHandler,
  Logger,
} from '@nestjs/common';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import * as fs from 'fs';
import * as path from 'path';
import * as moment from 'moment';
import * as uuid from 'uuid';

@Injectable()
export class LoggingInterceptor implements NestInterceptor {
  private readonly logger = new Logger('Request');

  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    const request = context.switchToHttp().getRequest();
    const response = context.switchToHttp().getResponse();

    const requestId = uuid.v4();
    const requestTime = moment().format('DD-MM-YYYY_HH-mm-ss');

    const logsDirectory = path.join(__dirname, '../..', 'logs');
    if (!fs.existsSync(logsDirectory)) {
      fs.mkdirSync(logsDirectory);
    }

    const logFilePath = path.join(
      __dirname,
      '../..',
      'logs',
      `${requestTime}.log`,
    );

    const logData = {
      requestId,
      method: request.method,
      url: request.url,
      query: request.query,
      params: request.params,
      body: request.body,
    };

    fs.appendFileSync(logFilePath, JSON.stringify(logData) + '\n');

    return next.handle().pipe(
      tap((responseData) => {
        const logResponseData = {
          requestId,
          status: response.statusCode,
          body: responseData,
        };

        fs.appendFileSync(logFilePath, JSON.stringify(logResponseData) + '\n');
      }),
    );
  }
}
