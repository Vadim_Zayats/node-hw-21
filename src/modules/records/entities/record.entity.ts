import { Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Post {
  @PrimaryGeneratedColumn()
  id: number;
  @IsString()
  name: string;
  @IsString()
  content: string;
}

import { IsString } from 'class-validator';
